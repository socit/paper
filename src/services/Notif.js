
let _notif;

function setTopLevelNotif(notifRef) {
  _notif = notifRef;
}

function notify(message, time = 2000) {
  _notif.notify(message, time);
}

const Notif = {
  notify,
  setTopLevelNotif
};

export { Notif }