
let _dialog;

function setTopLevelDialog(dialogRef) {
  _dialog = dialogRef;
}

function open(dialog) {
  const promise = _dialog.open(dialog);
  return promise;
}

function close() {
  _dialog.close();
}

function isOpen() {
  return _dialog.isOpen();
}

const Dialog = {
  open,
  close,
  isOpen,
  setTopLevelDialog
};

export { Dialog }