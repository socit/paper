import React from 'react';
import { ReactPersianDatepickerInput } from '@socit/react-persian-datepicker';

import './Input.scss';

function Input(props) {
    let refInput = null;

    let main = (
        <input
            className="input-main-input"
            ref={ref => { refInput = ref; }}
            type={props.type || 'text'}
            value={props.value}
            onChange={e => { props.onChange(e.target.value); }}
            autoFocus={props.autoFocus}
        />
    );
    if (props.type === 'date') {
        main = (
            <ReactPersianDatepickerInput
                style={{
                    width: '100%',
                    height: '100%',
                    lineHeight: '44px',
                    backgroundColor: 'transparent',
                    fontSize: '14px',
                    textAlign: 'left',
                    color: 'rgb(51 , 57 , 88 )',
                    padding: 0,
                    border: 'none',
                    borderRadius: '0',
                    direction: 'ltr',
                    zIndex: '2'
                }}
                onChange={props.onChange}
            />
        );
    } else if (props.type === 'textarea') {
        main = (
            <textarea
                ref={ref => { refInput = ref; }}
                className="input-main-textarea"
                value={props.value}
                onChange={e => { props.onChange(e.target.value); }}
                autoFocus={props.autoFocus}
            ></textarea>
        );
    }

    return (
        <div className={`input ${props.borderBottom ? 'border-bottom' : ''}`}>
            <div
                className="input-title"
                onClick={function() {
                    if (refInput) {
                        refInput.focus();
                    }
                }}
            >
                {props.titleIcon &&
                    <div className="input-icon">
                        <i className="material-icons">{props.titleIcon}</i>
                    </div>
                }
                <span>{props.titleText}</span>
            </div>

            <div className="input-main">
                {main}

                { props.preview &&
                    <div className={`input-main-preview ${props.previewColor ? props.previewColor : 'green'}`}>
                        {props.preview}
                    </div>
                }
            </div>
        </div>
    );
}

export { Input };
