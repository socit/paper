import React, { Component } from 'react';

import './Dialog.scss';

class Dialog extends Component {
  constructor(props) {
    super(props);

    this.showStatus = 0; // -1: go hide, 0: hide, 1: show
    this.dialog = null;

    this.resolve = null;
    this.reject = null;
  }

  onDialogClick(event) {
    event.stopPropagation();
  }

  isOpen() {
    return this.showStatus === 0 ? false : true;
  }
  
  open(dialog) {
    let dialogClassName = 'dialog';
    if (dialog.props.className && dialog.props.className !== undefined) {
      dialogClassName = dialogClassName + " " + dialog.props.className
    }
    this.dialog = React.cloneElement(dialog, {className: dialogClassName, onClick: this.onDialogClick});

    this.showStatus = 1;
    this.props.routerBlur(true);
    this.forceUpdate();

    return {
      promise: new Promise((resolve, reject) => {
        this.resolve = resolve;
        this.reject = reject;
      }),
      resolve: this.resolve,
      reject: this.reject
    }
  }

  close() {
    this.showStatus = -1;
    this.props.routerBlur(false);
    this.forceUpdate();

    setTimeout(() => {
      this.showStatus = 0;
      this.props.routerBlur(false);
      this.forceUpdate();

      this.resolve({ action: 'dismiss' });
    }, 300);
  }

  render() {
    if (this.showStatus === 0) {
      return null;
    } else {
      let containerClassName = 'dialog-container';
      if (this.showStatus === 1) {
        containerClassName = containerClassName + ' show';
      } else if (this.showStatus === -1) {
        containerClassName = containerClassName + ' hide';
      }

      return (
        <div className={containerClassName} onClick={this.close.bind(this)}>
          {this.dialog}
        </div>
      );
    }
  }
}

export { Dialog };
