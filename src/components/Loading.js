import React from 'react';
import PropTypes from 'prop-types';

import './Loading.scss';

function Loading(props) {
    return (
        <div className="loading">
            <div className={`loading-spinner ${props.size}`} />
        </div>
    );
}

Loading.propTypes = {
	size : PropTypes.oneOf([ 'small', 'medium', 'large' ])
};

export { Loading }
