import React, { Component } from 'react';

import './Notif.scss';

class Notif extends Component {
  constructor(props) {
    super(props);

    this.isShow = false;
    this.message = "";
  }

  notify(message, time = 3000) {
    if (!this.isShow) {
      this.isShow = true;
      this.message = message;
      setTimeout(() => {
        this.isShow = false;
        this.forceUpdate();
      }, time);
      this.forceUpdate();
    }
  }

  dismiss() {

  }

  render() {
    let className = "notif";
    if (this.isShow) {
      className = className + " notif-show";
    }

    return (
      <div className={className}>
        <div className="notif-text">
          {this.message}
        </div>
      </div>
    )
  }
}

export {Notif};
