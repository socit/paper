import React, { useState } from 'react';

import './Account.scss';

function Account(props) {
    const [ menuShow, setMenuShow ] = useState(false);

    const user = props.firebase.auth().currentUser;
    let name = null;
    let email = null;
    let photoUrl = null;
    if (user) {
        name = user.displayName;
        email = user.email;
        photoUrl = user.photoURL;
    } else {
    // No user is signed in.
    }

    return (
        <div className="account">
            <div className="account-image" onClick={() => { setMenuShow(!menuShow); }}>
                {photoUrl ?
                    <img src={photoUrl} alt={name ? name : email} />
                    :
                    <div className="account-image-placeholder">
                        <i className="material-icons">person</i>
                    </div>
                }
            </div>

            {menuShow ?
                <div className="account-menu">
                    <div className="account-menu-name">
                        {name ? name : email}
                    </div>

                    <div
                        className="account-menu-item"
                        onClick={() => {
                            props.firebase.auth().signOut().then(function() {
                                console.log('Successful Sign Out');
                            }).catch(function(error) {
                                console.log(error);
                            });
                        }}
                    >
                        <i className="material-icons">power_settings_new</i>
                        <span>Sign Out</span>
                    </div>
                </div>
                :
                null
            }
        </div>
    );
}

export { Account };
