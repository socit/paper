import React from 'react';

import './Select.scss';

function Select(props) {
    let refInput = null;

    return (
        <div className={`select ${props.borderBottom ? 'border-bottom' : ''}`}>
            <div
                className="select-title"
                onClick={function() {
                    if (refInput) {
                        refInput.focus();
                    }
                }}
            >
                {props.titleIcon &&
                    <div className="select-icon">
                        <i className="material-icons">{props.titleIcon}</i>
                    </div>
                }
                <span>{props.titleText}</span>
            </div>

            <div className="select-main">
                <select
                    onChange={e => {
                        let item = {};
                        for (let i = 0; i < props.options.length; i++) {
                            if (props.options[i]._id === e.target.value) {
                                item = props.options[i];
                            }
                        }
                        props.onChange(item);
                    }}
                    value={props.value._id}
                >
                    {
                        props.options.map(item => (
                            <option value={item._id}>{item.name}</option>
                        ))
                    }
                </select>
            </div>
        </div>
    );
}

export { Select };
