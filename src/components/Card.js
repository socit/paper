import React from 'react';
import PropTypes from 'prop-types';

import './Card.scss';

function Card(props) {
	return (
		<div
			className={`card ${props.className ? props.className : ''}`}
			id={props.id ? props.id : ''}
			style={props.style ? props.style : {}}
		>
			<div className="card-header">
				<div className="card-title">
					{props.title}
				</div>

				{props.onAdd ?
					<div className="card-header-button green" onClick={props.onAdd}>
						<i className="material-icons">add</i>
					</div>
					:
					null
				}

				{props.onEdit ?
					<div className="card-header-button blue" onClick={props.onEdit}>
						<i className="material-icons">edit</i>
					</div>
					:
					null
				}

				{props.onSwitch ?
					<div className="card-header-button purple" onClick={props.onSwitch}>
						{/* <i className="material-icons">compare_arrows</i> */}
						<i className="material-icons">swap_horiz</i>
					</div>
					:
					null
				}
			</div>

			<div className="card-main">
				{props.children}
			</div>
		</div>
	);
}

Card.propTypes = {
	className : PropTypes.string,
	id        : PropTypes.string,
	style     : PropTypes.object,
	title     : PropTypes.string,

	onAdd     : PropTypes.func,
	onEdit    : PropTypes.func,
	onSwitch  : PropTypes.func
};

export { Card };
