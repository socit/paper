import React from 'react';
import {
	Dialog
} from '../services';

import './Action.scss';

function Action_(props) {
	return (
		<div className={`${props.className} dialog-action`} onClick={props.onClick}>
			<div className="dialog-action-title">
				{props.title}
			</div>

            <div className="dialog-action-description">
				{props.description}
			</div>

			<div className="dialog-action-buttons-container">
				<form
					action="#"
					onSubmit={(e) => {
						e.preventDefault();

                        resolve({ action: 'primary' });
					}}
				>
					<input
                        className={`dialog-action-button accept ${props.primaryColor || 'green'}`}
                        type="submit"
                        value={props.primaryText || 'Accept'}
                        autoFocus
                        onKeyUp={(e) => {
                            if (e.key === "Escape") {
                                resolve({ action: 'secondary' });
                            }
                        }}
                    />

                    <button
                        className="dialog-action-button dismiss"
                        onClick={() => {
                            resolve({ action: 'secondary' });
                        }}
                    >
                        {props.secondaryText || 'Dismiss'}
                    </button>
				</form>
			</div>
		</div>
	);
}

let resolve = null;
let reject = null;

const open = (title, description, primaryText, secondaryText, primaryColor) => {
	const { promise, resolve: resolve_, reject: reject_ } = Dialog.open(
        <Action_
            title={title}
            description={description}
            primaryText={primaryText}
            secondaryText={secondaryText}
            primaryColor={primaryColor}
        />
    );
	resolve = resolve_;
	reject = reject_;

	return promise;
};

const close = () => {
    Dialog.close();
};

const isOpen = () => {
    return Dialog.isOpen();
}

const Action = {
    open,
    close,
    isOpen
};

export { Action };