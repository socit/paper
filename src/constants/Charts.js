
export const CHART_TYPES = {
    'line': { symbols: ['x', 'y'] },
    'candlestick': { symbols: ['x', 'low', 'open', 'close', 'high'] }
};
