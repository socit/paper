import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Home from './screens/Home';

function Router(props) {
	return (
		<BrowserRouter>
			<Switch>
				<Route path='/' exact render={(routerProps) => <Home {...routerProps} firebase={props.firebase} />} />
			</Switch>
		</BrowserRouter>
	);
}

export default Router;
