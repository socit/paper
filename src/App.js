import React, { useRef, useEffect, useState } from 'react';
import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import Router from './Router';
import {
  Notif as NotifService,
  Dialog as DialogService
} from './services';
import {
  Notif,
  Dialog
} from './components';

import Auth from './screens/Auth';

import './App.scss';

function App() {
  const [ authState, setAuthState ] = useState(0); // 0: nothing, -1: not logged in, 1: logged in
  const refRouter = useRef(null);


  useEffect(() => {
    const firebaseConfig = {
      apiKey: "AIzaSyAtLSS0Zze_XsEYQNOhtAUQlM6gh4nze0w",
      authDomain: "paper-7b108.firebaseapp.com",
      databaseURL: "https://paper-7b108.firebaseio.com",
      projectId: "paper-7b108",
      storageBucket: "",
      messagingSenderId: "448452505196",
      appId: "1:448452505196:web:5809bd68a518a9903eb249",
      measurementId: "G-ZQYXK6LGHN"
    };
    firebase.initializeApp(firebaseConfig);

    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        setAuthState(1);
      } else {
        setAuthState(-1);
      }
    });
  }, []);


  let body = (
    <div id="app-initial">
      <div id="app-initial-loading" />
      <div id="app-initial-loading-icon">
        <i className="material-icons">person</i>
      </div>
    </div>
  );
  if (authState === -1) {
    body = <Auth firebase={firebase} />;
  } else if (authState === 1) {
    body = (
      <div style={{ width: '100%', height: '100%' }}>
        <div style={{ width: '100%', height: '100%' }} ref={refRouter}>
          <Router firebase={firebase} />
        </div>

        <Notif ref={refNotif => { NotifService.setTopLevelNotif(refNotif); }} />
    
        <Dialog
          ref={refDialog => { DialogService.setTopLevelDialog(refDialog); }}
          routerBlur={state => {
            if (state) {
              refRouter.current.classList.add('router-blur');
            } else {
              refRouter.current.classList.remove('router-blur');
            }
          }}
        />
      </div>
    );
  }

  return (
    <div id="app">
      {body}
    </div>
  );
}

export default App;
