import React, { Component } from 'react';

import './index.scss';

import logo from '../../assets/images/logo.png';
import googleLogo from '../../assets/images/googleLogo.png';

class Auth extends Component {
    constructor(props) {
        super(props);

        this.onAuthenticateClick = this.onAuthenticateClick.bind(this);
    }

    onAuthenticateClick() {
        const provider = new this.props.firebase.auth.GoogleAuthProvider();

        this.props.firebase.auth().signInWithPopup(provider).then(function(result) {
            // This gives you a Google Access Token. You can use it to access the Google API.
            // var token = result.credential.accessToken;
            // The signed-in user info.
            // var user = result.user;
            // ...
        }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(errorCode);
            console.log(errorMessage);
            // The email of the user's account used.
            // var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            // var credential = error.credential;
            // ...
        });
    }

    render() {
        return (
            <div id="auth">
                <div id="auth-logo-container">
                    <img src={logo} alt="paper logo" />
                </div>

                <div
                    id="auth-login-button"
                    onClick={this.onAuthenticateClick}
                >
                    <img src={googleLogo} alt="google logo" />

                    <span>Login With Google Account</span>
                </div>
            </div>
        );
    }
}

export default Auth;
