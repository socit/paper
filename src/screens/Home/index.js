import React, { Component } from 'react';
import {
	Account,
	Loading
} from '../../components';
import {
	Action
} from '../../dialogs';
import {
	Notif
} from '../../services';
import {
	Tab
} from './components';
import {
	TabAdd,
	TabEdit
} from './dialogs';

import './index.scss';

class Home extends Component {
	constructor(props) {
		super(props);

		this.state = {
			// tabs: {
			// 	'1': {
			// 		name: 'Tab 1',
			// 		charts: {
			// 			'1': {
			// 				name: 'chart 2',
			// 				color: '#f00',
			// 				type: 'line',
			// 				data: [
			// 					{ x: [1398, 4, 21], y: 1 },
			// 					{ x: [1398, 4, 22], y: 2 },
			// 					{ x: [1398, 4, 23], y: 3 },
			// 					{ x: [1398, 4, 24], y: 4 },
			// 					{ x: [1398, 4, 25], y: 5 },
			// 					{ x: [1398, 4, 26], y: 6 },
			// 					{ x: [1398, 4, 27], y: 7 },
			// 					{ x: [1398, 4, 28], y: 6 }
			// 				]
			// 			},
			// 			'2': {
			// 				name: 'chart 3',
			// 				color: '#0f0',
			// 				type: 'line',
			// 				data: [
			// 					{ x: [1398, 4, 21], y: 3 },
			// 					{ x: [1398, 4, 22], y: 5 },
			// 					{ x: [1398, 4, 23], y: 6 },
			// 					{ x: [1398, 4, 24], y: 7 },
			// 					{ x: [1398, 4, 25], y: 8 },
			// 					{ x: [1398, 4, 26], y: 9 },
			// 					{ x: [1398, 4, 27], y: 10 },
			// 					{ x: [1398, 4, 28], y: 13 }
			// 				]
			// 			}
			// 		},
			// 		candlesticksDay: {
			// 			type: 'candlestick',
			// 			data: [
			// 				{ x: [1398, 6, 1], open: 2, close: 3, high: 4, low: 1 },
			// 				{ x: [1398, 6, 2], open: 3, close: 4, high: 5, low: 2 },
			// 				{ x: [1398, 6, 3], open: 4, close: 6, high: 7, low: 2 },
			// 				{ x: [1398, 6, 4], open: 5, close: 3, high: 6, low: 2 },
			// 				{ x: [1398, 6, 5], open: 4, close: 3, high: 5, low: 1 },
			// 				{ x: [1398, 6, 6], open: 4, close: 5, high: 6, low: 2 },
			// 				{ x: [1398, 6, 7], open: 4, close: 5, high: 7, low: 3 },
			// 				{ x: [1398, 6, 8], open: 5, close: 6, high: 6, low: 4 },
			// 				{ x: [1398, 6, 9], open: 6, close: 3, high: 7, low: 3 }
			// 			]
			// 		},
			// 		numbers: {
			// 			price: 3200,
			// 			epsT: 300,
			// 			epsE: 350
			// 		},
			// 		estimates: {
			// 			'1': { name: 'MACD', buyDate: null, buyPrice: 900, sellDate: null, sellPrice: 1000 },
			// 			'2': { name: 'Fibonacci', buyDate: null, buyPrice: 900, sellDate: null, sellPrice: 1000 },
			// 			'3': { name: 'Fib Trend', buyDate: null, buyPrice: 900, sellDate: null, sellPrice: 1000 }
			// 		},
			// 		myTrade: {
			// 			buy: '12000',
			// 			sell: '14000'
			// 		},
			// 		info: {
			// 			'1': { state: { _id: '0', name: 'unknown' } , title: 'عنوان ۱', description: 'متنش برای توضیح اینکه این نماد به چه دلیل ممکنه افزایش یا کاهش قیمت داشته باشه' },
			// 			'2': { state: { _id: '-1', name: 'bad' } , title: 'عنوان یکم طولانی تر ۲', description: 'متنش برای توضیح اینکه این نماد به چه دلیل ممکنه افزایش یا کاهش قیمت داشته باشه' },
			// 			'3': { state: { _id: '-1', name: 'bad' } , title: 'عنوان ۳', description: 'متنش برای توضیح اینکه این نماد به چه دلیل ممکنه افزایش یا کاهش قیمت داشته باشه' },
			// 			'4': { state: { _id: '1', name: 'good' } , title: 'عنوان ۴', description: 'متنش برای توضیح اینکه این نماد به چه دلیل ممکنه افزایش یا کاهش قیمت داشته باشه' },
			// 			'5': { state: { _id: '1', name: 'good' } , title: 'عنوان ۵', description: 'متنش برای توضیح اینکه این نماد به چه دلیل ممکنه افزایش یا کاهش قیمت داشته باشه' },
			// 			'6': { state: { _id: '0', name: 'unknown' } , title: 'عنوان ۶', description: 'متنش برای توضیح اینکه این نماد به چه دلیل ممکنه افزایش یا کاهش قیمت داشته باشه' },
			// 			'7': { state: { _id: '-1', name: 'bad' } , title: 'عنوان ۷', description: 'متنش برای توضیح اینکه این نماد به چه دلیل ممکنه افزایش یا کاهش قیمت داشته باشه' }
			// 		}
			// 	},
			// 	'2': {
			// 		name: 'Tab 2',
			// 		charts: {},
			// 		candlesticksDay: { type: 'candlestick', data: [] },
			// 		numbers: { price: 0, epsT: 0, epsE: 0 },
			// 		estimates: {
			// 			'1': { name: 'MACD', buyDate: null, buyPrice: 850, sellDate: null, sellPrice: 9500 },
			// 			'2': { name: 'Fib Trend', buyDate: null, buyPrice: 800, sellDate: null, sellPrice: 950 }
			// 		},
			// 		myTrade: {
			// 			buy: '',
			// 			sell: ''
			// 		},
			// 		info: {
			// 			'1': { state: { _id: '-1', name: 'bad' } , title: 'عنوان ۱', description: 'متنش برای توضیح اینکه این نماد به چه دلیل ممکنه افزایش یا کاهش قیمت داشته باشه' },
			// 			'2': { state: { _id: '-1', name: 'bad' } , title: 'عنوان ۲', description: 'متنش برای توضیح اینکه این نماد به چه دلیل ممکنه افزایش یا کاهش قیمت داشته باشه' }
			// 		}
			// 	},
			// 	'3': {
			// 		name: 'Tab 3',
			// 		charts: {},
			// 		candlesticksDay: { type: 'candlestick', data: [] },
			// 		numbers: { price: 0, epsT: 0, epsE: 0 },
			// 		estimates: {},
			// 		myTrade: { buy: '13000', sell: '' },
			// 		info: {}
			// 	}
			// },
			tabs: {},
			tabKey: null,

			tabsFetch: false
		};

		this.onTabEditClick = this.onTabEditClick.bind(this);
		this.tabAdd = this.tabAdd.bind(this);
		this.tabSave = this.tabSave.bind(this);
		this.handleKeyUp = this.handleKeyUp.bind(this);

		this.refTab = React.createRef();
	}

	componentDidMount() {
		document.addEventListener('keyup', this.handleKeyUp);

		this.setState({ tabsFetch: true }, () => {
			const userId = this.props.firebase.auth().currentUser.uid;
			this.props.firebase.database().ref('tabs').orderByChild("owner").equalTo(userId)
				.once("value").then((snapshot) => {
					console.log(snapshot.val());
					this.setState({ tabs: snapshot.val(), tabsFetch: false });
				});
		});
	}

	componentWillUnmount() {
		document.removeEventListener('keyup', this.handleKeyUp);
	}

	handleKeyUp(e) {
		if (e.key === "Dead") {
			let tabStatus = 0;
			if (this.refTab.current) {
				tabStatus = this.refTab.current.getStatus();
			}

			if (tabStatus === 0) {
				this.tabAdd();
			} else if (tabStatus === 1) {
				this.refTab.current.save(
					'Before adding new tab, need to save current tab.',
					this.tabAdd
				);
			}
		}
	}

	async onTabEditClick() {
        try {
			const { tabs, tabKey } = this.state;
            const response = await TabEdit.open(tabs[tabKey].name);

            if (response.action === 'accept') {
				if (tabs[tabKey].name !== response.data.name) {
					const newTabs = { ...tabs };

					await this.props.firebase.database().ref('/tabs/' + tabKey)
						.update({
							name: response.data.name
						});

					newTabs[tabKey].name = response.data.name;
	
					this.setState({ tabs: newTabs }, () => {
						TabEdit.close();
					});
				} else {
					TabEdit.close();
				}
			} else if (response.action === 'delete') {
				const newTabs = { ...tabs };

				await this.props.firebase.database().ref('/tabs/' + tabKey).remove();

				delete newTabs[tabKey];

				let newTabKey = null;
				const keys = Object.keys(newTabs);
				if (keys.length > 0) {
					newTabKey = keys[keys.length - 1];
				}

				this.setState({ tabs: newTabs, tabKey: newTabKey }, () => {
					TabEdit.close();
				});
			}
		} catch (error) {
			console.log(error);
			Notif.notify('Problem with edit tab.');
        }
	}

	async tabAdd() {
        try {
            const response = await TabAdd.open();

            if (response.action === 'accept') {
				const { tabs } = this.state;
				const newTabs = { ...tabs };

				const newItem = {
					name           : response.data.name,
					charts         : {},
					candlesticksDay: { type: 'candlestick', data: [] },
					numbers: { price: 0, epsT: 0, epsE: 0 },
					estimates      : {},
					myTrade        : { buy: '', sell: '' },
					info           : {}
				};

				const userId = this.props.firebase.auth().currentUser.uid;
				const tabsRef = this.props.firebase.database().ref('tabs');
				const newTabRef = tabsRef.push();
				await newTabRef.set({
					...newItem,
					owner: userId
				});

				newTabs[newTabRef.key] = newItem;
				this.setState({ tabs: newTabs, tabKey: newTabRef.key }, () => {
					TabAdd.close();
				});
			}
        } catch (error) {
            console.log(error);
        }
	}

	async tabSave(data, description, callback) {
		try {
			const response = await Action.open(
				'Tab',
				description,
				'Save',
				'Close',
				'green'
			);

			if (response.action === 'primary') {
				const { tabs, tabKey } = this.state;

				await this.props.firebase.database().ref('/tabs/' + tabKey)
					.update({
						...data
					});

				const newTabs = { ...tabs };
				newTabs[tabKey] = {
					name: newTabs[tabKey].name,
					...data
				};

				this.setState({ tabs: newTabs }, () => {
					Action.close();
					setTimeout(callback, 300);
				});

			} else if (response.action === 'secondary') {
				Action.close();
			}
		} catch (error) {
			console.log(error);
			Notif.notify('Problem with saving tab data.');
		}
	}

	renderTabButtons() {
		const { tabs, tabKey } = this.state;

		const headerItems = [];
		for (let key in tabs) {
			const activeClass = key === tabKey ? 'active' : '';
			headerItems.push(
				<div
					key={key}
					className={`home-tab-button ${activeClass}`}
					onClick={() => {
						if (tabKey !== key) {
							let tabStatus = 0;
							if (this.refTab.current) {
								tabStatus = this.refTab.current.getStatus();
							}

							if (tabStatus === 0) {
								this.setState({ tabKey: key });
							} else if (tabStatus === 1) {
								this.refTab.current.save(
									'Before changing active tab, need to save current tab.',
									() => {
										this.setState({ tabKey: key });
									}
								);
							}
						}
					}}
				>
					{/* {(key === tabKey && (this.refTab.current && this.refTab.current.getStatus() === 1)) ?
						<div className="home-tab-button-not-saved" />
						:
						null
					} */}

					<div className="home-tab-button-name">{tabs[key].name}</div>

					<div className="home-tab-button-edit" onClick={this.onTabEditClick}>
						<i className="material-icons">edit</i>
					</div>
				</div>
			);
		}

		return (
			<div id="home-tab-buttons-container">
				{headerItems}
			</div>
		);
	}

	render() {
		const { tabs, tabKey, tabsFetch } = this.state;

		return (
			<div id="home">
				<Account
					firebase={this.props.firebase}
				/>

				<div id="home-header">
					{tabsFetch ?
						<div id="home-header-loading">
							<Loading size="small" />
						</div>
						:
						this.renderTabButtons()
					}

					<div
						id="home-tab-add-button"
						onClick={() => {
							let tabStatus = 0;
							if (this.refTab.current) {
								tabStatus = this.refTab.current.getStatus();
							}

							if (tabStatus === 0) {
								this.tabAdd();
							} else if (tabStatus === 1) {
								this.refTab.current.save(
									'Before adding new tab, need to save current tab.',
									this.tabAdd
								);
							}
						}}
					>
						<i className="material-icons">add</i>
					</div>
				</div>

				{ (tabKey && tabs[tabKey]) ?
					<Tab
						ref={this.refTab}
						key={tabKey} // change tab component when new key come
						data={tabs[tabKey]}
						onSave={this.tabSave}
					/>
					:
					null
				}
			</div>
		);
	}
}

export default Home;
