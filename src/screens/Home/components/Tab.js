import React, { Component } from 'react';
import {
    Action
} from '../../../dialogs';
import {
    Charts,
    CandlesticksDay,
    Numbers,
    Estimates,
	MyTrade,
	Info
} from './';
import {
    InfoAdd,
	EstimateAdd,
	MyTradeEdit
} from '../dialogs';
import {
	Card
} from '../../../components';

import './Tab.scss';

class Tab extends Component {
    constructor(props) {
        super(props);

        this.state = {
            charts: props.data.charts || {},
            candlesticksDay: {
                type: 'candlestick',
                data: props.data.candlesticksDay.data || []
            },
            numbers: {
                price: props.data.numbers.price,
                epsT: props.data.numbers.epsT
            },
            estimates: { ...props.data.estimates },
            myTrade: {
                buy: props.data.myTrade.buy,
                sell: props.data.myTrade.sell
            },
            info: { ...props.data.info },

            status: 0 // 0: normal, 1: something to save
        };

        this.onInfoAddClick = this.onInfoAddClick.bind(this);
		this.onEstimateAddClick = this.onEstimateAddClick.bind(this);
        this.onMyTradeEditClick = this.onMyTradeEditClick.bind(this);
        this.getStatus = this.getStatus.bind(this);
        this.refreshData = this.refreshData.bind(this);
        this.discard = this.discard.bind(this);
		this.save = this.save.bind(this);
    }

    getStatus() {
        return this.state.status;
    }

    refreshData() {
        this.setState({
            charts: this.props.data.charts || {},
            candlesticksDay: this.props.data.candlesticksDay || { type: 'candlestick', data: [] },
            numbers: {
                price: this.props.data.numbers.price,
                epsT: this.props.data.numbers.epsT
            },
            estimates: { ...this.props.data.estimates },
            myTrade: {
                buy: this.props.data.myTrade.buy,
                sell: this.props.data.myTrade.sell
            },
            info: { ...this.props.data.info },
        });
    }

    async discard(description, callback) {
		try {
			const response = await Action.open(
				'Tab',
				description,
				'Discard',
				'Close',
				'purple'
			);

			if (response.action === 'primary') {
				this.setState({ status: 0 }, () => {
					this.refreshData();
					Action.close();
					setTimeout(callback, 300);
				});
			} else if (response.action === 'secondary') {
				Action.close();
			}
		} catch (err) {
			console.log(err);
		}
	}

    save(description, callback) {
        this.props.onSave(
            {
                charts: this.state.charts,
                candlesticksDay: this.state.candlesticksDay,
                numbers: this.state.numbers,
                estimates: this.state.estimates,
                myTrade: this.state.myTrade,
                info: this.state.info
            },
            description,
            callback
        );
    }
    
    async onInfoAddClick() {
        try {
            const response = await InfoAdd.open();

            if (response.action === 'accept') {
				const newItem = response.data;
                
                const newInfo = { ...this.state.info };

                let key = '1';
                const keys = Object.keys(newInfo);
                if (keys.length > 0) {
                    key = (parseInt(keys[keys.length - 1]) + 1) + '';
                }

                newInfo[key] = newItem;

				this.setState({ info: newInfo, status: 1 }, () => {
					InfoAdd.close();
				});
			}
        } catch (error) {
            console.log(error);
        }
	}

	async onEstimateAddClick() {
        try {
            const response = await EstimateAdd.open();

            if (response.action === 'accept') {
				const newItem = response.data;
                
                const newEstimates = { ...this.state.estimates };

                let key = '1';
                const keys = Object.keys(newEstimates);
                if (keys.length > 0) {
                    key = (parseInt(keys[keys.length - 1]) + 1) + '';
                }
				
                newEstimates[key] = newItem;

				this.setState({ estimates: newEstimates, status: 1 }, () => {
					EstimateAdd.close();
				});
			}
        } catch (error) {
            console.log(error);
        }
	}
	
	async onMyTradeEditClick() {
        try {
            const response = await MyTradeEdit.open(this.state.myTrade.buy, this.state.myTrade.sell);

            if (response.action === 'accept') {    
				this.setState({ myTrade: response.data, status: 1 }, () => {
					MyTradeEdit.close();
				});
			}
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        const { charts, candlesticksDay, numbers, estimates, myTrade, info, status } = this.state;

        return (
            <div id="home-tab">
                <Charts
                    charts={charts}
                    onChange={(newCharts) => {
                        this.setState({ charts: newCharts, status: 1 });
                    }}
                />

                <CandlesticksDay
                    chart={candlesticksDay}
                    onChange={(newChart) => {
                        this.setState({ candlesticksDay: newChart, status: 1 });
                    }}
                />

                <Numbers
                    numbers={numbers}
                    onChange={(newNumbers) => {
                        this.setState({ numbers: newNumbers, status: 1 });
                    }}
                />

                <Card
                    title="Estimates"
                    id="home-estimates-card"
                    style={{ flex: 1 }}
                    onAdd={this.onEstimateAddClick}
                >
                    <Estimates
                        items={estimates}
                        onItemDelete={(key) => {
                            const newEstimates = { ...this.state.estimates };

                            delete newEstimates[key];

                            this.setState({ estimates: newEstimates, status: 1 });
                        }}
                    />
                </Card>

                <Card
                    title="Buy / Sell"
                    id="home-my-trade-card"
                    style={{ flex: 1 }}
                    onEdit={this.onMyTradeEditClick}
                >
                    <MyTrade
                        buy={myTrade.buy}
                        sell={myTrade.sell}
                    />
                </Card>
    
                <Card
                    title="Info"
                    id="home-info-card"
                    style={{ flex: 1 }}
                    onAdd={this.onInfoAddClick}
                >
                    <Info
                        items={info}
                        onItemDelete={(key) => {
                            const newInfo = { ...this.state.info };
                            
                            delete newInfo[key];

                            this.setState({ info: newInfo, status: 1 });
                        }}
                    />
                </Card>

                { status === 1 ?
					<div
						id="home-tab-discard-button"
						onClick={() => {
							this.discard(
								'Are you sure for discarding changes on this tab ?',
								() => {}
							);
						}}
						title="Discard Changes"
					>
                        <i className="material-icons">block</i>
                    </div>
                    :
                    null
                }

				{ status === 1 ?
					<div
						id="home-tab-save-button"
						onClick={() => {
							this.save(
								'Are you sure for saving this tab ?',
								() => {
                                    this.setState({ status: 0 });
                                }
							);
						}}
						title="Save Tab"
					>
                        <i className="material-icons">save</i>
                    </div>
                    :
                    null
                }
            </div>
        );
    }
}

export { Tab };
