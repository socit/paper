import React from 'react';

import './MyTrade.scss';

function MyTrade(props) {
    return (
        <div className="home-my-trade">
            <div className="home-my-trade-item red">
                <div className="home-my-trade-item-text">{props.buy}</div>
                <div className="home-my-trade-item-tag">B</div>
            </div>

            <div className="home-my-trade-item green">
                <div className="home-my-trade-item-text">{props.sell}</div>
                <div className="home-my-trade-item-tag">S</div>
            </div>
        </div>
    );
}

export { MyTrade };
