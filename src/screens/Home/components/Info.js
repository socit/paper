import React from 'react';

import './Info.scss';

function Info(props) {
    const { items, onItemDelete } = props;

    const itemsRender = [];
    for (let key in items) {
        let icon = '';
        let color = '';
        if (items[key].state._id === '-1') {
            icon = 'close';
            color = 'red';
        } else if (items[key].state._id === '0') {
            icon = 'remove';
            color = 'gray';
        } else if (items[key].state._id === '1') {
            icon = 'done';
            color = 'green';
        }

        itemsRender.push(
            <div key={key} className="home-info-item">
                <div className={`home-info-item-state ${color}`}>
                    <i className="material-icons">{icon}</i>
                </div>
                <div className="home-info-item-title">{items[key].title}</div>
                <div className="home-info-item-description">{items[key].description}</div>

                <div
                    className="home-info-item-delete"
                    onClick={() => {
                        onItemDelete(key);
                    }}
                >
                    <i className="material-icons">delete</i>
                </div>
            </div>
        );
    }

    return (
        <div className="home-info">
            {itemsRender}
        </div>
    );
}

export { Info };
