import React, { Component } from 'react';
import ReactChart from '@socit/react-chart';
import {
    Card
} from '../../../components';
import {
    ChartData
} from './';
import {
    ChartAdd
} from '../dialogs';

import './Charts.scss';

class Charts extends Component {
    constructor(props) {
        super(props);

        this.state = {
            charts: props.charts, // charts state will have new refrence on change, also every chart will have new refrence on change
            activeChartKey: null,
            editMode: false,
            dimensions: { width: 0, height: 0 }
        };

        this.refCharts = React.createRef();

        this.onSwitchClick = this.onSwitchClick.bind(this);
        this.onListAddClick = this.onListAddClick.bind(this);
    }

    componentDidMount() {
        this.setState({
            dimensions: {
				width: this.refCharts.current.offsetWidth,
				height: this.refCharts.current.offsetHeight
			}
        });
    }

    onSwitchClick() {
        this.setState(
            state => {
                return {
                    editMode: !state.editMode
                }
            },
            () => {
                if (!this.state.editMode && (this.props.charts !== this.state.charts)) { // editMode was true
                    this.props.onChange(this.state.charts);
                }
            }
        );
    }
    
    async onListAddClick() {
        try {
            const response = await ChartAdd.open();
            if (response.action === 'accept') {
                const newCharts = { ...this.state.charts };

                const newItem = {
                    name: response.data.name,
                    color: response.data.color,
                    type: response.data.type,
                    data: []
                };

                let key = '1';
                const keys = Object.keys(newCharts);
                if (keys.length > 0) {
                    key = (parseInt(keys[keys.length - 1]) + 1) + '';
                }

                newCharts[key] = newItem;

                this.setState({ charts: newCharts }, () => {
                    ChartAdd.close();
                });
            }
        } catch (error) {
            console.log(error);
        }
    }

    render() {

        /**
         * TODO:
         *  - ReactChart data structure is not compatible with this application.
         *  - This solution (chartShow) must be temporary.
         * 
         */
        const chartsShow = [];

        const chartsRender = [];
        for (let key in this.state.charts) {
            chartsShow.push({
                color: this.state.charts[key].color,
                type: this.state.charts[key].type,
                data: this.state.charts[key].data
            });

            chartsRender.push(
                <div
                    key={key}
                    className={`home-charts-edit-list-item ${this.state.activeChartKey === key ? 'active' : ''}`}
                    onClick={() => { this.setState({ activeChartKey: key }); }}
                >
                    <div className="home-charts-edit-list-item-color" style={{ backgroundColor: this.state.charts[key].color }} />

                    {this.state.charts[key].name}

                    <div
                        className="home-charts-edit-list-item-delete"
                        onClick={(e) => {
                            e.stopPropagation();
                            const newCharts = { ...this.state.charts };
                            delete newCharts[key];
                            this.setState({ charts: newCharts, activeChartKey: null }); 
                        }}
                    >
                        <i className="material-icons">delete</i>
                    </div>
                </div>
            );
        }

        return (
            <Card
                title="Charts"
                className="home-charts-card"
                onSwitch={this.onSwitchClick}
            >
                <div className="home-charts" ref={this.refCharts}>
                    { this.state.editMode ?
                        <div className="home-charts-edit">
                            <div className="home-charts-edit-left">
                                <div className="home-charts-edit-list">
                                    {chartsRender}
                                </div>

                                <div
                                    className="home-charts-edit-list-add"
                                    onClick={this.onListAddClick}
                                >
                                    <i className="material-icons">add</i>
                                    <span>add chart</span>
                                </div>
                            </div>

                            <div className="home-charts-edit-right">
                                {this.state.activeChartKey !== null ?
                                    <ChartData
                                        chart={this.state.charts[this.state.activeChartKey]}
                                        onChange={(newChart) => {
                                            const newCharts = { ...this.state.charts };
                                            newCharts[this.state.activeChartKey] = newChart;
                                            this.setState({ charts: newCharts });
                                        }}
                                    />
                                    :
                                    null
                                }
                            </div>
                        </div>
                        :
                        Object.keys(this.state.charts).length > 0 ?
                            <ReactChart
                                width={this.state.dimensions.width}
                                height={this.state.dimensions.height}
                                axisX={true}
                                axisY={true}
                                // charts={[
                                //     { type: 'line', data: data }
                                // ]}
                                charts={chartsShow}
                            />
                            :
                            <div style={{ width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                <div onClick={this.onSwitchClick}>No Data</div>
                            </div>
                    }
                </div>
            </Card>
        );
    }
}

export { Charts };
