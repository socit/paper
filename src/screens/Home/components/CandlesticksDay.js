import React, { Component } from 'react';
import ReactChart from '@socit/react-chart';
import {
    Card
} from '../../../components';
import {
    ChartData
} from './';

import './CandlesticksDay.scss';

class CandlesticksDay extends Component {
    constructor(props) {
        super(props);

        this.state = {
            chart: props.chart,
            dimensions: { width: 0, height: 0 },
            editMode: false
        };

        this.refCandlesticks = React.createRef();

        this.onSwitchClick = this.onSwitchClick.bind(this);
    }

    componentDidMount() {
        this.setState({
            dimensions: {
				width: this.refCandlesticks.current.offsetWidth,
				height: this.refCandlesticks.current.offsetHeight
			}
        });
    }

    onSwitchClick() {
        this.setState(
            state => {
                return {
                    editMode: !state.editMode
                }
            },
            () => {
                if (!this.state.editMode) { // editMode was true
                    this.props.onChange(this.state.chart);
                }
            }
        );
    }

    render() {
        return (
            <Card
                title="Candlesticks (Day)"
                id="home-candlesticks-day-card"
                // style={{ flex: 1 }}
                onSwitch={this.onSwitchClick}
            >
                <div className="home-candlesticks-day" ref={this.refCandlesticks}>
                    { this.state.editMode ?
                        <div className="home-candlesticks-day-edit">
                            <ChartData
                                chart={this.state.chart}
                                onChange={(newChart) => {
                                    this.setState({ chart: newChart });
                                }}
                            />
                        </div>
                        :
                        this.state.chart.data.length > 0 ?
                            <ReactChart
                                width={this.state.dimensions.width}
                                height={this.state.dimensions.height}
                                axisX={true}
                                axisY={false}
                                charts={[
                                    this.state.chart
                                ]}
                            />
                            :
                            <div style={{ width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                <div onClick={this.onSwitchClick}>No Data</div>
                            </div>
                    }
                </div>
            </Card>
        );
    }
}

export { CandlesticksDay };
