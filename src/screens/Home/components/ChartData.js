import React from 'react';
import {
    ChartRowAdd
} from '../dialogs';
import {
    CHART_TYPES
} from '../../../constants';

import './ChartData.scss';

function ChartData(props) {
    let symbols = [];
    if (CHART_TYPES[props.chart.type]) {
        symbols = CHART_TYPES[props.chart.type].symbols;
    }

    return (
        <div className="home-chart-data">
            <div className="home-chart-data-symbols-container">
                {symbols.map((item, index) => {
                    return (
                        <div key={index} className="home-chart-data-symbol">
                            {item}
                        </div>
                    );
                })}
            </div>

            <div className="home-chart-data-numbers-container">
                {
                    props.chart.data.map((row, index) => {
                        return (
                            <div key={index} className={`home-chart-data-number-row ${index === props.chart.data.length - 1 ? '' : 'border'}`}>
                                {symbols.map((symbol, symbolIndex) => {
                                    let text = row[symbol];
                                    if (Array.isArray(row[symbol])) {
                                        text = row[symbol][0] + ' / ' + row[symbol][1] + ' / ' + row[symbol][2];
                                    }
                                    return (
                                        <div key={symbolIndex} className="home-chart-data-number-cell">{text}</div>
                                    );
                                })}

                                {/* Delete Buttons */}
                                <div
                                    className="home-chart-data-number-row-delete"
                                    onClick={() => {
                                        const newChart = { ...props.chart };

                                        newChart.data = [ ...newChart.data ];
                                        newChart.data.splice(index, 1);
            
                                        props.onChange(newChart);
                                    }}
                                >
                                    <i className="material-icons">delete</i>
                                </div>

                                {/* Movement Buttons */}
                                {index === 0 ? null :
                                    <div
                                        className="home-chart-data-number-row-move-left"
                                        onClick={() => {
                                            const newChart = { ...props.chart };

                                            newChart.data = [ ...newChart.data ];
                                            const tmp = newChart.data[index];
                                            newChart.data[index] = newChart.data[index - 1];
                                            newChart.data[index - 1] = tmp;

                                            props.onChange(newChart);
                                        }}
                                    >
                                        <i className="material-icons">chevron_left</i>
                                    </div>
                                }

                                {index === props.chart.data.length - 1 ? null :                                
                                    <div
                                        className="home-chart-data-number-row-move-right"
                                        onClick={() => {
                                            const newChart = { ...props.chart };

                                            newChart.data = [ ...newChart.data ];
                                            const tmp = newChart.data[index];
                                            newChart.data[index] = newChart.data[index + 1];
                                            newChart.data[index + 1] = tmp;
                
                                            props.onChange(newChart);
                                        }}
                                    >
                                        <i className="material-icons">chevron_right</i>
                                    </div>
                                }
                            </div>
                        );
                    })
                }
            </div>

            <div
                className="home-chart-data-add"
                onClick={async () => {
                    try {
                        const response = await ChartRowAdd.open(props.chart.type);
                        if (response.action === 'accept') {
                            const newChart = { ...props.chart };

                            newChart.data = [ ...newChart.data ];
                            newChart.data.push(response.data);

                            props.onChange(newChart);
                            ChartRowAdd.close();
                        }
                    } catch (error) {
                        console.log(error);
                    }
                }}
            >
                <i className="material-icons">add</i>
                {/* <span>add row</span> */}
            </div>
        </div>
    );
}

export { ChartData };
