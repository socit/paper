import React from 'react';
import {
    Card
} from '../../../components';
import {
    NumbersEdit
} from '../dialogs';

import './Numbers.scss';

function Numbers(props) {
    let PE = 0;
    if (props.numbers.price !== 0 && props.numbers.epsT !== 0) {
        PE = props.numbers.price / props.numbers.epsT;
    }

    // let intrinsicPrice = 0;
    // let intrinsicPriceDiffPercent = 0;
    // if (PE !== 0 && props.numbers.epsE !== 0) {
    //     intrinsicPrice = parseInt(PE * props.numbers.epsE);
    //     intrinsicPriceDiffPercent = (((intrinsicPrice - props.numbers.price) / props.numbers.price) * 100).toFixed(2);
    // }


    // let intrinsicPriceColor = 'gray';
    // if (intrinsicPriceDiffPercent > 5) {
    //     intrinsicPriceColor = 'green';
    // } else if (intrinsicPriceDiffPercent < -5) {
    //     intrinsicPriceColor = 'red';
    // }

    return (
        <Card
            title="Numbers"
            id="home-numbers-card"
            style={{ flex: 1 }}
            onEdit={async () => {
                try {
                    const response = await NumbersEdit.open(props.numbers.price, props.numbers.epsT/*, props.numbers.epsE*/);
                    if (response.action === 'accept') {
                        props.onChange(response.data);
                        NumbersEdit.close();
                    }
                } catch (error) {
                    console.log(error);
                }
            }}
        >
            <div className="home-numbers">
                <div className="home-numbers-left">
                    <div className="home-numbers-item border-bottom">
                        <div className="home-numbers-item-name">Price</div>
                        <div className="home-numbers-item-value">{props.numbers.price}</div>
                    </div>

                    <div className="home-numbers-item border-bottom">
                        <div className="home-numbers-item-name">EPS (TTM)</div>
                        <div className="home-numbers-item-value">{props.numbers.epsT}</div>
                    </div>

                    <div className="home-numbers-item border-bottom">
                        <div className="home-numbers-item-name">P/E (TTM)</div>
                        <div className="home-numbers-item-value gray">{PE.toFixed(2)}</div>
                    </div>

                    <div className="home-numbers-item">
                        <div className="home-numbers-item-name">EPS (Est)</div>
                        {/* <div className="home-numbers-item-value">{props.numbers.epsE}</div> */}
                        <div className="home-numbers-item-value">coming soon ...</div>
                    </div>
                </div>

                <div className="home-numbers-right">
                    <div className="home-numbers-intrinsic-price">
                        <div className="home-numbers-intrinsic-price-name">Value</div>
                        {/* <div className={`home-numbers-intrinsic-price-value ${intrinsicPriceColor}`}>
                            {intrinsicPrice}
                            <span className="home-numbers-intrinsic-price-value-percent">
                                ({intrinsicPriceDiffPercent > 0 ? '+' : ''}{intrinsicPriceDiffPercent}%)
                            </span>
                        </div> */}
                        <div className="home-numbers-intrinsic-price-value gray">
                            0
                            <span className="home-numbers-intrinsic-price-value-percent">
                                (0%)
                            </span>
                        </div>
                    </div>
                </div>

            </div>
        </Card>
    );
}

export { Numbers };
