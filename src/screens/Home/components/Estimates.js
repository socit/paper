import React from 'react';

import './Estimates.scss';

function Estimates(props) {
    const { items, onItemDelete } = props;

    const itemsRender = [];
    for (let key in items) {
        itemsRender.push(
            <div key={key} className="home-estimates-main-item">
                <div className="home-estimates-main-item-cell">{items[key].name}</div>
                <div className="home-estimates-main-item-cell">{items[key].buyDate ? `${items[key].buyDate[0]} / ${items[key].buyDate[1]} / ${items[key].buyDate[2]}` : '---'}</div>
                <div className="home-estimates-main-item-cell">{items[key].buyPrice === '' ? '---' : items[key].buyPrice}</div>
                <div className="home-estimates-main-item-cell">{items[key].sellDate ? `${items[key].sellDate[0]} / ${items[key].sellDate[1]} / ${items[key].sellDate[2]}` : '---'}</div>
                <div className="home-estimates-main-item-cell">{items[key].sellPrice === '' ? '---' : items[key].sellPrice}</div>

                <div className="home-estimates-main-item-delete" onClick={() => { onItemDelete(key); }}>
                    <i className="material-icons">delete</i>
                </div>
            </div>
        );
    }

    return (
        <div className="home-estimates">
            <div className="home-estimates-header">
                <div className="home-estimates-header-text">Name</div>
                <div className="home-estimates-header-line" />
                <div className="home-estimates-header-text">Buy Date</div>
                <div className="home-estimates-header-line" />
                <div className="home-estimates-header-text">Buy Price</div>
                <div className="home-estimates-header-line" />
                <div className="home-estimates-header-text">Sell Date</div>
                <div className="home-estimates-header-line" />
                <div className="home-estimates-header-text">Sell Price</div>
            </div>

            <div className="home-estimates-main">
                {itemsRender}
            </div>
        </div>
    );
}

export { Estimates };
