import React, { useState } from 'react';
import {
	Input
} from '../../../components';
import {
	Dialog,
	Notif
} from '../../../services';

import './EstimateAdd.scss';

function EstimateAdd_(props) {
	const [name, setName] = useState('');
	const [buyDate, setBuyDate] = useState(null);
	const [buyPrice, setBuyPrice] = useState('');
	const [sellDate, setSellDate] = useState(null);
	const [sellPrice, setSellPrice] = useState('');

	return (
		<div className={`${props.className} home-dialog-estimate-add`} onClick={props.onClick}>
			<div className="home-dialog-estimate-add-header">
				New Estimate
			</div>

			<div className="home-dialog-estimate-add-main">
				<form
					action="#"
					onSubmit={(e) => {
						e.preventDefault();

						if (name.length > 0) {
							resolve({
								action: 'accept',
								data: {
									name,
									buyDate,
									buyPrice,
									sellDate,
									sellPrice
								}
							});
						} else {
							Notif.notify('Check Fields');
						}
					}}
				>
					<Input
						titleText="Name"
						titleIcon="title"
						value={name}
						onChange={function(value) { setName(value); }}
						borderBottom
						preview={name.length > 0 ? null : <div>Required</div>}
						previewColor="red"
						autoFocus
					/>

					<Input
						titleText="Buy Date"
						titleIcon="date_range"
						type="date"
						value={buyDate}
						onChange={function(value) { setBuyDate(value); }}
						borderBottom
					/>

					<Input
						titleText="Buy Price"
						titleIcon="attach_money"
						value={buyPrice}
						onChange={function(value) { setBuyPrice(value); }}
						borderBottom
					/>

					<Input
						titleText="Sell Date"
						titleIcon="date_range"
						type="date"
						value={sellDate}
						onChange={function(value) { setSellDate(value); }}
						borderBottom
					/>

					<Input
						titleText="Sell Price"
						titleIcon="attach_money"
						value={sellPrice}
						onChange={function(value) { setSellPrice(value); }}
					/>

					{/* <div className="home-dialog-estimate-add-submit">
						Add
					</div> */}
					<input className="home-dialog-estimate-add-submit" type="submit" value="Add" />
				</form>
			</div>
		</div>
	);
}

let resolve = null;
let reject = null;

const open = () => {
	const { promise, resolve: resolve_, reject: reject_ } = Dialog.open(<EstimateAdd_ />);
	resolve = resolve_;
	reject = reject_;

	return promise;
};

const close = () => {
    Dialog.close();
};

const isOpen = () => {
    return Dialog.isOpen();
}

const EstimateAdd = {
    open,
    close,
    isOpen
};

export { EstimateAdd };