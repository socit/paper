import React, { useState } from 'react';
import {
	Input,
	Select
} from '../../../components';
import {
	Dialog,
	Notif
} from '../../../services';

import './InfoAdd.scss';

function InfoAdd_(props) {
	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');
	const [state, setState] = useState({ _id: '0', name: 'unknown' });

	return (
		<div className={`${props.className} home-dialog-info-add`} onClick={props.onClick}>
			<div className="home-dialog-info-add-header">
				New Info
			</div>

			<div className="home-dialog-info-add-main">
				<form
					action="#"
					onSubmit={(e) => {
						e.preventDefault();

						if (title.length > 0 && description.length > 0) {
							resolve({
								action: 'accept',
								data: {
									title,
									description,
									state
								}
							});
						} else {
							Notif.notify('Check Fields');
						}
					}}
				>
					<Input
						titleText="Title"
						titleIcon="title"
						value={title}
						onChange={function(value) { setTitle(value); }}
						preview={title.length > 0 ? null : <div>Required</div>}
						previewColor="red"
						autoFocus
					/>

					<Input
						titleText="Desc"
						titleIcon="title"
						value={description}
						onChange={function(value) { setDescription(value); }}
						preview={description.length > 0 ? null : <div>Required</div>}
						previewColor="red"
					/>

					<Select
						titleText="state"
						titleIcon="adjust"
						value={state}
						options={[{ _id: '-1', name: 'bad' }, { _id: '0', name: 'unknown' }, { _id: '1', name: 'good' }]}
						onChange={function(value) {
							setState(value);
						}}
					/>

					{/* <div className="home-dialog-info-add-submit">
						Add
					</div> */}
					<input className="home-dialog-info-add-submit" type="submit" value="Add" />
				</form>
			</div>
		</div>
	);
}

let resolve = null;
let reject = null;

const open = () => {
	const { promise, resolve: resolve_, reject: reject_ } = Dialog.open(<InfoAdd_ />);
	resolve = resolve_;
	reject = reject_;

	return promise;
};

const close = () => {
    Dialog.close();
};

const isOpen = () => {
    return Dialog.isOpen();
}

const InfoAdd = {
    open,
    close,
    isOpen
};

export { InfoAdd };