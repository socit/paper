import React, { useState } from 'react';
import {
	Input
} from '../../../components';
import {
	Dialog
} from '../../../services';

import './MyTradeEdit.scss';

function MyTradeEdit_(props) {
	const [buy, setBuy] = useState(props.buy);
	const [sell, setSell] = useState(props.sell);

	return (
		<div className={`${props.className} home-dialog-my-trade-edit`} onClick={props.onClick}>
			<div className="home-dialog-my-trade-edit-header">
				Edit My-Trade
			</div>

			<div className="home-dialog-my-trade-edit-main">
				<form
					action="#"
					onSubmit={(e) => {
                        e.preventDefault();
                        
                        resolve({
                            action: 'accept',
                            data: {
                                buy,
                                sell
                            }
                        });
					}}
				>
					<Input
						titleText="Buy Price"
						titleIcon="attach_money"
						value={buy}
						onChange={function(value) { setBuy(value); }}
						autoFocus
					/>

					<Input
						titleText="Sell Price"
						titleIcon="attach_money"
						value={sell}
						onChange={function(value) { setSell(value); }}
					/>

					<input className="home-dialog-my-trade-edit-submit" type="submit" value="Edit" />
				</form>
			</div>
		</div>
	);
}

let resolve = null;
let reject = null;

const open = (buy, sell) => {
	const { promise, resolve: resolve_, reject: reject_ } = Dialog.open(<MyTradeEdit_ buy={buy} sell={sell} />);
	resolve = resolve_;
	reject = reject_;

	return promise;
};

const close = () => {
    Dialog.close();
};

const isOpen = () => {
    return Dialog.isOpen();
}

const MyTradeEdit = {
    open,
    close,
    isOpen
};

export { MyTradeEdit };