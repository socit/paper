import React, { useState } from 'react';
import {
	Input
} from '../../../components';
import {
	Dialog,
	Notif
} from '../../../services';

import './TabAdd.scss';

function TabAdd_(props) {
	const [name, setName] = useState('');

	return (
		<div className={`${props.className} home-dialog-tab-add`} onClick={props.onClick}>
			<div className="home-dialog-tab-add-header">
				New Tab
			</div>

			<div className="home-dialog-tab-add-main">
				<form
					action="#"
					onSubmit={(e) => {
						e.preventDefault();

						if (name.length > 0) {
							resolve({
								action: 'accept',
								data: {
									name
								}
							});
						} else {
							Notif.notify('Check Fields');
						}
					}}
				>
					<Input
						titleText="Name"
						titleIcon="title"
						value={name}
						onChange={function(value) { setName(value); }}
						preview={name.length > 0 ? null : <div>Required</div>}
						previewColor="red"
						autoFocus
					/>

					{/* <div className="home-dialog-tab-add-submit">
						Add
					</div> */}
					<input className="home-dialog-tab-add-submit" type="submit" value="Add" />
				</form>
			</div>
		</div>
	);
}

let resolve = null;
let reject = null;

const open = () => {
	const { promise, resolve: resolve_, reject: reject_ } = Dialog.open(<TabAdd_ />);
	resolve = resolve_;
	reject = reject_;

	return promise;
};

const close = () => {
    Dialog.close();
};

const isOpen = () => {
    return Dialog.isOpen();
}

const TabAdd = {
    open,
    close,
    isOpen
};

export { TabAdd };