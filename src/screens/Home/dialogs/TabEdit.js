import React, { useState } from 'react';
import {
	Input
} from '../../../components';
import {
	Dialog,
	Notif
} from '../../../services';

import './TabEdit.scss';

function TabEdit_(props) {
	const [name, setName] = useState(props.name);

	return (
		<div className={`${props.className} home-dialog-tab-edit`} onClick={props.onClick}>
			<div className="home-dialog-tab-edit-header">
				<div className="home-dialog-tab-edit-title">Edit Tab</div>

                <div
                    className="home-dialog-tab-edit-delete-button"
                    onClick={() => {
                        resolve({
                            action: 'delete'
                        });
					}}
                >
                    <i className="material-icons">delete</i>
                </div>
			</div>

			<div className="home-dialog-tab-edit-main">
				<form
					action="#"
					onSubmit={(e) => {
						e.preventDefault();

						if (name.length > 0) {
							resolve({
								action: 'accept',
								data: {
									name
								}
							});
						} else {
							Notif.notify('Check Fields');
						}
					}}
				>
					<Input
						titleText="Name"
						titleIcon="title"
						value={name}
						onChange={function(value) { setName(value); }}
						preview={name.length > 0 ? null : <div>Required</div>}
						previewColor="red"
						autoFocus
					/>

					{/* <div className="home-dialog-tab-edit-submit">
						Edit
					</div> */}
					<input className="home-dialog-tab-edit-submit" type="submit" value="Edit" />
				</form>
			</div>
		</div>
	);
}

let resolve = null;
let reject = null;

const open = (name) => {
	const { promise, resolve: resolve_, reject: reject_ } = Dialog.open(<TabEdit_ name={name} />);
	resolve = resolve_;
	reject = reject_;

	return promise;
};

const close = () => {
    Dialog.close();
};

const isOpen = () => {
    return Dialog.isOpen();
}

const TabEdit = {
    open,
    close,
    isOpen
};

export { TabEdit };