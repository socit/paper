import React, { useState } from 'react';
import {
	Input
} from '../../../components';
import {
	Dialog
} from '../../../services';

import './NumbersEdit.scss';

function NumbersEdit_(props) {
	const [price, setPrice] = useState(props.price);
	const [epsT, setEpsT] = useState(props.epsT);
	// const [epsE, setEpsE] = useState(props.epsE);

	return (
		<div className={`${props.className} home-dialog-numbers-edit`} onClick={props.onClick}>
			<div className="home-dialog-numbers-edit-header">
				Edit Numbers
			</div>

			<div className="home-dialog-numbers-edit-main">
				<form
					action="#"
					onSubmit={(e) => {
                        e.preventDefault();
                        
                        resolve({
                            action: 'accept',
                            data: {
                                price,
                                epsT,
                                // epsE
                            }
                        });
					}}
				>
					<Input
						titleText="Price"
						titleIcon="attach_money"
						value={price}
						onChange={function(value) { setPrice(value); }}
						autoFocus
					/>

					<Input
						titleText="eps (TTM)"
						titleIcon="attach_money"
						value={epsT}
						onChange={function(value) { setEpsT(value); }}
					/>

                    {/* <Input
						titleText="eps (Est)"
						titleIcon="attach_money"
						value={epsE}
						onChange={function(value) { setEpsE(value); }}
					/> */}

					<input className="home-dialog-numbers-edit-submit" type="submit" value="Edit" />
				</form>
			</div>
		</div>
	);
}

let resolve = null;
let reject = null;

const open = (price, epsT, epsE) => {
	const { promise, resolve: resolve_, reject: reject_ } = Dialog.open(<NumbersEdit_ price={price} epsT={epsT} epsE={epsE} />);
	resolve = resolve_;
	reject = reject_;

	return promise;
};

const close = () => {
    Dialog.close();
};

const isOpen = () => {
    return Dialog.isOpen();
}

const NumbersEdit = {
    open,
    close,
    isOpen
};

export { NumbersEdit };