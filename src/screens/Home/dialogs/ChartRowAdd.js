import React, { useState } from 'react';
import {
	Input
} from '../../../components';
import {
	Dialog
} from '../../../services';
import {
    CHART_TYPES
} from '../../../constants';

import './ChartRowAdd.scss';

function ChartRowAdd_(props) {
    let items = {};
    for (let i = 0; i < CHART_TYPES[props.type].symbols.length; i++) {
        items[CHART_TYPES[props.type].symbols[i]] = 0;
    }
    if (items.x) {
        items.x = null;
    }
	const [data, setData] = useState(items);

	return (
		<div className={`${props.className} home-dialog-chart-row-add`} style={{ height: CHART_TYPES[props.type].symbols.length * 50 + 48 + 56 + 'px' }} onClick={props.onClick}>
			<div className="home-dialog-chart-row-add-header">
				New Chart Row
			</div>

			<div className="home-dialog-chart-row-add-main" style={{ height: CHART_TYPES[props.type].symbols.length * 50 + 48 + 'px' }}>
				<form
					action="#"
					onSubmit={(e) => {
						e.preventDefault();

                        resolve({
                            action: 'accept',
                            data
                        });
					}}
				>
                    {CHART_TYPES[props.type].symbols.map((item, index) => {
                        if (item === 'x') {
                            return (
                                <Input
                                    titleText={item}
                                    titleIcon="date_range"
                                    type="date"
                                    value={data[item]}
                                    onChange={function(value) { setData({ ...data, [item]: value }); }}
                                    borderBottom={index !== CHART_TYPES[props.type].symbols.length - 1}
                                    autoFocus
                                />
                            );
                        }
                        return (
                            <Input
                                titleText={item}
                                titleIcon="title"
                                value={data[item]}
                                onChange={function(value) { setData({ ...data, [item]: parseInt(value) }); }}
                                borderBottom={index !== CHART_TYPES[props.type].symbols.length - 1}
                                autoFocus
                            />
                        );
                    })}

					<input className="home-dialog-chart-row-add-submit" type="submit" value="Add" />
				</form>
			</div>
		</div>
	);
}

let resolve = null;
let reject = null;

const open = (type) => {
	const { promise, resolve: resolve_, reject: reject_ } = Dialog.open(<ChartRowAdd_ type={type} />);
	resolve = resolve_;
	reject = reject_;

	return promise;
};

const close = () => {
    Dialog.close();
};

const isOpen = () => {
    return Dialog.isOpen();
}

const ChartRowAdd = {
    open,
    close,
    isOpen
};

export { ChartRowAdd };