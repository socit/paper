import React, { useState } from 'react';
import {
	Input,
	Select
} from '../../../components';
import {
	Dialog,
	Notif
} from '../../../services';
import {
    CHART_TYPES
} from '../../../constants';

import './ChartAdd.scss';

function ChartAdd_(props) {
	const typesArray = [];
	let typesIndex = 0;
	for (const key in CHART_TYPES) {
		typesArray.push({ _id: typesIndex + '', name: key });
		typesIndex++;
	}

	const [name, setName] = useState('');
	const [color, setColor] = useState('#000000');
	const [type, setType] = useState(typesArray[0]);

	return (
		<div className={`${props.className} home-dialog-chart-add`} onClick={props.onClick}>
			<div className="home-dialog-chart-add-header">
				New Chart
			</div>

			<div className="home-dialog-chart-add-main">
				<form
					action="#"
					onSubmit={(e) => {
						e.preventDefault();

						if (name.length > 0) {
							resolve({
								action: 'accept',
								data: {
									name,
									color,
									type: type.name
								}
							});
						} else {
							Notif.notify('Check Fields');
						}
					}}
				>
					<Input
						titleText="Name"
						titleIcon="title"
						value={name}
						onChange={function(value) { setName(value); }}
						borderBottom
						preview={name.length > 0 ? null : <div>Required</div>}
						previewColor="red"
						autoFocus
					/>

					<Input
						titleText="Color"
						titleIcon="color_lens"
						type="color"
						value={color}
						onChange={function(value) { setColor(value); }}
						borderBottom
					/>

					<Select
						titleText="Type"
						titleIcon="list"
						value={type}
						options={typesArray}
						onChange={function(value) {
							setType(value);
						}}
					/>

					<input className="home-dialog-chart-add-submit" type="submit" value="Add" />
				</form>
			</div>
		</div>
	);
}

let resolve = null;
let reject = null;

const open = () => {
	const { promise, resolve: resolve_, reject: reject_ } = Dialog.open(<ChartAdd_ />);
	resolve = resolve_;
	reject = reject_;

	return promise;
};

const close = () => {
    Dialog.close();
};

const isOpen = () => {
    return Dialog.isOpen();
}

const ChartAdd = {
    open,
    close,
    isOpen
};

export { ChartAdd };