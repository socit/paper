import React, { Component } from 'react';
import {
	Card,
	Chart
} from '../../components';
import {
	Estimates,
	MyTrade,
	Info
} from './components';

import './index.scss';

class Home extends Component {
	constructor(props) {
		super(props);

		this.state = {
			homeChartsTopDimension: { width: 0, height: 0 },
			homeCandlesticksDay: { width: 0, height: 0 },
			homeCandlesticksHour: { width: 0, height: 0 }
		};

		this.refHomeChartsTop = React.createRef();
		this.refHomeCandlesticksDay = React.createRef();
		this.refHomeCandlesticksHour = React.createRef();
	}

	componentDidMount() {
		this.setState({
			homeChartsTopDimension: {
				width: this.refHomeChartsTop.current.offsetWidth,
				height: this.refHomeChartsTop.current.offsetHeight
			},
			homeCandlesticksDay: {
				width: this.refHomeCandlesticksDay.current.offsetWidth,
				height: this.refHomeCandlesticksDay.current.offsetHeight
			},
			homeCandlesticksHour: {
				width: this.refHomeCandlesticksHour.current.offsetWidth,
				height: this.refHomeCandlesticksHour.current.offsetHeight
			}
		});
	}

	render() {
		// const data4 = [
		// 	{x: new Date(2016, 6, 1), open: 5, close: 10, high: 15, low: 0},
		// 	{x: new Date(2016, 6, 2), open: 10, close: 15, high: 20, low: 5},
		// 	{x: new Date(2016, 6, 3), open: 15, close: 20, high: 22, low: 10},
		// 	{x: new Date(2016, 6, 4), open: 20, close: 10, high: 25, low: 7},
		// 	{x: new Date(2016, 6, 5), open: 10, close: 8, high: 15, low: 5},
		// 	{x: new Date(2016, 6, 5), open: 5, close: 15, high: 25, low: 0}
		// ];
		const data = [];
		for (let i = 0; i < 6; i++) {
			data.push({ x: i, y: i * i });
		}
		for (let i = 6; i < 12; i++) {
			data.push({ x: i, y: i });
		}
		for (let i = 12; i < 18; i++) {
			data.push({ x: i, y: (18 - i) * (18 - i) });
		}
	
		const data2 = [
			{ x: 1, open: 2, close: 3, high: 4, low: 1 },
			{ x: 2, open: 3, close: 4, high: 5, low: 2 },
			{ x: 3, open: 4, close: 6, high: 7, low: 2 },
			{ x: 4, open: 5, close: 3, high: 6, low: 2 },
			{ x: 5, open: 4, close: 3, high: 5, low: 1 },
			{ x: 6, open: 4, close: 5, high: 6, low: 2 },
			{ x: 7, open: 4, close: 5, high: 7, low: 3 },
			{ x: 8, open: 5, close: 6, high: 6, low: 4 },
			{ x: 9, open: 6, close: 3, high: 7, low: 3 }
		];
		
		return (
			<div id="home">
				<div id="home-header">
	
				</div>
	
				<div id="home-main">
					<div className="home-row" style={{ flex: 2, margin: '0 0 16px 0' }}>
						<div className="home-column" style={{ flex: 2, margin: '0 16px 0 0' }}>
							<Card
								title="Charts"
								id="home-charts"
								style={{ flex: 1 }}
								onEdit={() => {}}
							>
								<div style={{ width: '100%', height: '100%', display: 'flex', flexDirection: 'column' }}>
									<div id="home-charts-top" ref={this.refHomeChartsTop}>
										<Chart
											width={this.state.homeChartsTopDimension.width}
											height={this.state.homeChartsTopDimension.height}
											type="line"
											axisX={true}
											axisY={true}
											data={data}
										/>
									</div>
		
									<div id="home-charts-bottom">
										
									</div>
								</div>
							</Card>
						</div>
	
						<div className="home-column" style={{ flex: 1 }}>
							<div className="home-row" style={{ margin: '0 0 16px 0' }}>
								<div className="home-column">
									<Card
										title="Candlesticks (Day)"
										id="home-candlesticks-day"
										style={{ flex: 1 }}
										onEdit={() => {}}
									>
										<div style={{ width: '100%', height: '100%' }} ref={this.refHomeCandlesticksDay}>
											<Chart
												width={this.state.homeCandlesticksDay.width}
												height={this.state.homeCandlesticksDay.height}
												type="candlestick"
												axisX={true}
												axisY={true}
												data={data2}
											/>
										</div>
									</Card>
								</div>
							</div>
	
							<div className="home-row">
								<div className="home-column">
									<Card
										title="Candlesticks (Hour)"
										id="home-candlesticks-hour"
										style={{ flex: 1 }}
										onEdit={() => {}}
									>
										<div style={{ width: '100%', height: '100%' }} ref={this.refHomeCandlesticksHour}>
											<Chart
												width={this.state.homeCandlesticksHour.width}
												height={this.state.homeCandlesticksHour.height}
												type="candlestick"
												axisX={true}
												axisY={true}
												data={data2}
											/>
										</div>
									</Card>
								</div>
							</div>
						</div>
					</div>
	
					<div className="home-row">
						<div className="home-column" style={{ flex: 2, margin: '0 16px 0 0' }}>
							<div className="home-row">
								<div className="home-column" style={{ flex: 2, margin: '0 16px 0 0' }}>
									<Card
										title="Estimates"
										id="home-estimates"
										style={{ flex: 1 }}
										onEdit={() => {}}
									>
										<Estimates />
									</Card>
								</div>

								<div className="home-column" style={{ flex: 1 }}>
									<Card
										title="Buy / Sell"
										id="home-estimates"
										style={{ flex: 1 }}
										onEdit={() => {}}
									>
										<MyTrade />
									</Card>
								</div>
							</div>
						</div>
	
						<div className="home-column" style={{ flex: 1 }}>
							<Card
								title="Info"
								id="home-info"
								style={{ flex: 1 }}
								onEdit={() => {}}
							>
								<Info />
							</Card>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Home;
