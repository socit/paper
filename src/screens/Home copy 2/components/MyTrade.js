import React from 'react';

import './MyTrade.scss';

function MyTrade(props) {
    return (
        <div className="home-my-trade">
            <div className="home-my-trade-item" style={{ backgroundColor: 'rgba(230, 48, 52, 0.7)' }}>
                <div className="home-my-trade-item-text">1357</div>
                <div className="home-my-trade-item-tag" style={{ backgroundColor: 'rgba(230, 48, 52, 1)' }}>B</div>
            </div>

            <div className="home-my-trade-item" style={{ backgroundColor: 'rgba(16, 130, 66, 0.7)' }}>
                <div className="home-my-trade-item-text">1400</div>
                <div className="home-my-trade-item-tag" style={{ backgroundColor: 'rgba(16, 130, 66, 1)' }}>S</div>
            </div>
        </div>
    );
}

export { MyTrade };
