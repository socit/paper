import React from 'react';

import './Info.scss';

function Info(props) {
    const data = [
        { title: 'helloooo', description: 'something i want to say' },
        { title: 'helloooo', description: 'something i want to say' },
        { title: 'helloooo', description: 'something i want to say' },
        { title: 'helloooo', description: 'something i want to say' },
        { title: 'helloooo', description: 'something i want to say' }
    ];

    return (
        <div className="home-info">
            {data.map((item, index) => (
                <div key={index} className="home-info-item">
                    <div className="home-info-item-title">{item.title}</div>
                    <div className="home-info-item-description">{item.description}</div>
                </div>
            ))}
        </div>
    );
}

export { Info };
