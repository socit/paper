import React from 'react';

import './Estimates.scss';

function Estimates(props) {
    const data = [
        { name: 'MACD', buyDate: undefined, buyPrice: 900, sellDate: undefined, sellPrice: 1000 },
        { name: 'Fibonacci', buyDate: undefined, buyPrice: 900, sellDate: undefined, sellPrice: 1000 },
        { name: 'Fib Trend', buyDate: undefined, buyPrice: 900, sellDate: undefined, sellPrice: 1000 }
    ];
    return (
        <div className="home-estimates">
            <div className="home-estimates-header">
                <div className="home-estimates-header-text">Name</div>
                <div className="home-estimates-header-line" />
                <div className="home-estimates-header-text">Buy Date</div>
                <div className="home-estimates-header-line" />
                <div className="home-estimates-header-text">Buy Price</div>
                <div className="home-estimates-header-line" />
                <div className="home-estimates-header-text">Sell Date</div>
                <div className="home-estimates-header-line" />
                <div className="home-estimates-header-text">Sell Price</div>
            </div>

            <div className="home-estimates-main">
                {data.map(item => (
                    <div className="home-estimates-main-item">
                        <div>{item.name}</div>
                        <div>{item.buyDate}</div>
                        <div>{item.buyPrice}</div>
                        <div>{item.sellDate}</div>
                        <div>{item.sellPrice}</div>
                    </div>
                ))}
            </div>
        </div>
    );
}

export { Estimates };
