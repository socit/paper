import React from 'react';
import {
	VictoryChart,
	VictoryAxis,
	VictoryCandlestick,
	VictoryTheme,
	VictoryZoomContainer,
	VictoryContainer,
	VictoryLine
} from "victory";
import {
	Card
} from '../../components';

import './index.scss';

function Home(props) {
	const data = [
		{ x: new Date(2016, 6, 1), y: 2000 },
		{ x: new Date(2016, 6, 2), y: 2100 },
		{ x: new Date(2016, 6, 3), y: 2050 },
		{ x: new Date(2016, 6, 4), y: 1890 },
		{ x: new Date(2016, 6, 5), y: 1980 }
	];
	const data2 = [
		{ x: new Date(2016, 6, 1), y: 200 },
		{ x: new Date(2016, 6, 2), y: 210 },
		{ x: new Date(2016, 6, 3), y: 205 },
		{ x: new Date(2016, 6, 4), y: 189 },
		{ x: new Date(2016, 6, 5), y: 198 }
	];
	const data3 = [
		{ x: new Date(2016, 6, 1), y: 210 },
		{ x: new Date(2016, 6, 2), y: 225 },
		{ x: new Date(2016, 6, 3), y: 205 },
		{ x: new Date(2016, 6, 4), y: 180 },
		{ x: new Date(2016, 6, 5), y: 190 }
	];
	const data4 = [
		{x: new Date(2016, 6, 1), open: 5, close: 10, high: 15, low: 0},
		{x: new Date(2016, 6, 2), open: 10, close: 15, high: 20, low: 5},
		{x: new Date(2016, 6, 3), open: 15, close: 20, high: 22, low: 10},
		{x: new Date(2016, 6, 4), open: 20, close: 10, high: 25, low: 7},
		{x: new Date(2016, 6, 5), open: 10, close: 8, high: 15, low: 5},
		{x: new Date(2016, 6, 5), open: 5, close: 15, high: 25, low: 0}
	];
	
	return (
		<div id="home">
			<div id="home-header">

			</div>

			<div id="home-main">
				<div className="home-row" style={{ flex: 2, margin: '0 0 16px 0' }}>
					<div className="home-column" style={{ flex: 2, margin: '0 16px 0 0' }}>
						<Card
							title="Charts"
							id="home-charts"
							style={{ flex: 1 }}
							onEdit={() => {}}
						>
							<div id="home-charts-top">
								<VictoryChart
									height={100}
									padding={{ top: 10, right: 15, bottom: 5, left: 5 }}
									theme={VictoryTheme.material}
									domain={{y: [1800, 2200]}}
									scale={{ x: "time" }}
								>
									<VictoryAxis
										orientation="bottom"
										style={{
											axis: {stroke: "#fff"},
											grid: {stroke: "#f0f0f0"},
											ticks: {stroke: "#fff", size: 0},
											tickLabels: {fontSize: 0, padding: 0}
										}}
									/>
									<VictoryAxis
										dependentAxis
										offsetX={15}
										orientation="right"
										style={{
											axis: {stroke: "#ccc"},
											grid: {stroke: "#f0f0f0"},
											ticks: {stroke: "#f0f0f0", size: 4},
											tickLabels: {fontSize: 4, padding: 0}
										}}
									/>
									<VictoryLine
										interpolation="natural"
										style={{
											data: { stroke: "#bbb", strokeWidth: 1 }
										}}
										data={data}
									/>
								</VictoryChart>
							</div>

							<div id="home-charts-bottom">
								<VictoryChart
									height={50}
									padding={{ top: 5, right:15, bottom: 10, left: 5 }}
									theme={VictoryTheme.material}
									scale={{ x: "time" }}
									domain={{y: [170, 235]}}
									containerComponent={<VictoryContainer />}
								>
									<VictoryAxis
										// tickFormat={(t) => `${t.getDate()}/${t.getMonth()}`}
										orientation="bottom"
										style={{
											axis: {stroke: "#ccc"},
											grid: {stroke: "#f0f0f0"},
											ticks: {stroke: "#f0f0f0", size: 4},
											tickLabels: {fontSize: 4, padding: 0}
										}}
									/>
									<VictoryAxis
										dependentAxis
										offsetX={15}
										orientation="right"
										style={{
											axis: {stroke: "#ccc"},
											grid: {stroke: "#f0f0f0"},
											ticks: {stroke: "#f0f0f0", size: 4},
											tickLabels: {fontSize: 4, padding: 0}
										}}
									/>
									<VictoryLine
										interpolation="natural"
										style={{
											data: { stroke: "#0f0", strokeWidth: 0.5 }
										}}
										data={data2}
									/>
									<VictoryLine
										interpolation="natural"
										style={{
											data: { stroke: "#f00", strokeWidth: 0.5 }
										}}
										data={data3}
									/>
								</VictoryChart>
							</div>
						</Card>
					</div>

					<div className="home-column">
						<div className="home-row" style={{ margin: '0 0 16px 0' }}>
							<div className="home-column">
								<Card
									title="Candlestics (Day)"
									id="home-candlestics-day"
									style={{ flex: 1 }}
								>
									<VictoryChart
										width={1000}
										// height={200}
										padding={{ top: 0, right: 0, bottom: 0, left: 0 }}
										theme={VictoryTheme.material}
										domainPadding={{ x: 25 }}
										scale={{ x: "time" }}
										domain={{y: [0, 25]}}
										containerComponent={<VictoryChart />}
									>
										<VictoryAxis
											orientation="bottom"
											style={{
												axis: {stroke: "#ccc"},
												grid: {stroke: "#f0f0f0"},
												ticks: {stroke: "#f0f0f0", size: 4},
												tickLabels: {fontSize: 4, padding: 0}
											}}
										/>
										<VictoryCandlestick
											candleColors={{ positive: "rgb(62, 202, 24)", negative: "#c43a31" }}
											data={data4}
										/>
									</VictoryChart>
								</Card>
							</div>
						</div>

						<div className="home-row">
							<div className="home-column">
								<Card
									title="Candlestics (Hour)"
									id="home-candlestics-hour"
									style={{ flex: 1 }}
								>
									<VictoryChart
										width={1000}
										// height={200}
										padding={{ top: 0, right: 0, bottom: 0, left: 0 }}
										theme={VictoryTheme.material}
										domainPadding={{ x: 25 }}
										scale={{ x: "time" }}
										domain={{y: [0, 25]}}
										containerComponent={<VictoryChart />}
									>
										<VictoryAxis
											orientation="bottom"
											style={{
												axis: {stroke: "#ccc"},
												grid: {stroke: "#f0f0f0"},
												ticks: {stroke: "#f0f0f0", size: 4},
												tickLabels: {fontSize: 4, padding: 0}
											}}
										/>
										<VictoryCandlestick
											candleColors={{ positive: "rgb(62, 202, 24)", negative: "#c43a31" }}
											data={data4}
										/>
									</VictoryChart>
								</Card>
							</div>
						</div>
					</div>
				</div>

				<div className="home-row">
					<div className="home-column" style={{ flex: 2, margin: '0 16px 0 0' }}>
						<Card
							title="Estimates"
							id="home-estimates"
							style={{ flex: 1 }}
						>
							children
						</Card>
					</div>

					<div className="home-column">
						<Card
							title="Info"
							id="home-info"
							style={{ flex: 1 }}
						>
							children
						</Card>
					</div>
				</div>
			</div>
		</div>
	);
}

export default Home;
